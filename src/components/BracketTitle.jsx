import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import GameShape from './GameShape';
/**
 * The default title component used for each bracket, receives the game and the height of the bracket
 */
export default class BracketTitle extends PureComponent {
  static propTypes = {
    game:   GameShape.isRequired,
    height: PropTypes.number.isRequired
  };

  render() {
    const { game, height } = this.props;

    return (
      <h3 style={{ textAlign: 'center' }}>
        {game.bracketLabel || game.name} ({height} {height === 1 ? 'round' : 'rounds'})
      </h3>
    );
  }
}